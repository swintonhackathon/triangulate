﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;
namespace Triangulate1
{
    class Program
    {
        private List<Location> getCurrentLocations()
        {
            string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DB"];
            var server = MongoServer.Create(ConnectionString);
            var db = server.GetDatabase("wifitracker");
            var locationCollection = db.GetCollection<Location>("locations");
            var lastLocation = locationCollection.AsQueryable<Location>().OrderByDescending(m => m.timestamp).FirstOrDefault();

            var items = locationCollection.AsQueryable<Location>().Where(m => m.timestamp >= lastLocation.timestamp.AddMinutes(-1)).ToList();
            //Comment potentially remove dupplicates
            return items;
        }
        static void Main(string[] args)
        {
            //const string ConnectionString = "mongodb://localhost/?safe=true";
            string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DB"];
            var server = MongoServer.Create(ConnectionString);
            var db = server.GetDatabase("wifitracker");
            var probeCollection = db.GetCollection<Probe>("probes");
            var locationCollection = db.GetCollection<Location>("locations");


            while (true)
            {
                try
                {
                    System.Threading.Thread.Sleep(5000);
                    var lastprobe = db.GetCollection<Probe>("probes").AsQueryable<Probe>().OrderByDescending(m => m.timestamp).FirstOrDefault();
                    if (lastprobe == null)
                        continue;
                    Console.WriteLine(lastprobe.timestamp);
                    DateTime time = lastprobe.timestamp;

                    // db.GetCollection<Probe>("probes")
                    //var probes = probeCollection.AsQueryable<Probe>().Where(m=>  m.timestamp >= time.AddSeconds(-60)).ToList();

                    var probes = probeCollection.AsQueryable<Probe>().OrderByDescending(m => m.timestamp).Take(200).ToList();
                    probes = probes.Where(m => m.timestamp >= time.AddSeconds(-60)).ToList();
                    //.Where(m => m.timestamp >= tmpTime).ToList();
                    //var probes = probeCollection.Find(.AsQueryable<Probe>().Where(m=>  m.timestamp >= time.AddSeconds(-60)).ToList();

                    //var probes = db.GetCollection<Probe>("probes").Find(Query<Probe>.GT(m => m.timestamp, time.AddMinutes (-2))).ToList();


                    //Console.WriteLine(time.ToString() + " Items:" + probes.Count() + " devices: " + probes.Select(m => m.deviceid).Distinct ().Count());
                    foreach (var mac in probes.Select(m => m.mac).Distinct())
                    {
                        var macProbes = probes.Where(m => m.mac == mac).ToList();

                        if (macProbes.Any(m => m.timestamp > time.AddSeconds(-5)))
                            continue;

                        var macTimeStamp = macProbes.First().timestamp;

                        if (locationCollection.AsQueryable<Location>().Any(m => m.mac == mac && m.timestamp == macTimeStamp))
                            continue;


                        double x = 0;
                        double y = 0;
                        double totalStrength = 0;
                        foreach (var deviceID in macProbes.Select(m => m.deviceid).Distinct())
                        {
                            if (getX(deviceID) < 0)
                                continue;
                            var strength = 70 + macProbes.First(m => m.deviceid == deviceID).signalstrength;
                            totalStrength += strength;

                        }

                        foreach (var deviceID in probes.Where(m => m.mac == mac).Select(m => m.deviceid).Distinct())
                        {
                            var deviceX = getX(deviceID);
                            var deviceY = getY(deviceID);
                            if (deviceX < 0)
                                continue;

                            var strength = 70 + macProbes.First(m => m.deviceid == deviceID).signalstrength;

                            double multi = ((double)strength) / totalStrength;

                            x += multi * (double)deviceX;
                            y += multi * (double)deviceY;
                        }
                        var location = new Location()
                        {
                            timestamp = macTimeStamp,
                            mac = mac,
                            x = (int)x,
                            y = (int)y
                        };

                        Console.WriteLine(string.Format("{0} {1},{2}", mac, x, y));
                        locationCollection.Save(location);


                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //var query = probes.AsQueryable<Probe>().Where(m => m.TimeStamp > DateTime.Now.AddMinutes(-5));

        }
        private static int getX(string deviceID)
        {
            if (string.IsNullOrEmpty(deviceID))
            {
                Console.WriteLine("WARNING: A deviceID is not set!");
                return -1;
            }
            if (System.Configuration.ConfigurationManager.AppSettings["Node1-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node1-X"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node2-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node2-X"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node3-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node3-X"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node4-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node4-X"]);

            return -1;

        }
        private static int getY(string deviceID)
        {

            if (string.IsNullOrEmpty(deviceID))
            {
                Console.WriteLine("WARNING: A deviceID is not set!");
                return -1;
            }
            if (System.Configuration.ConfigurationManager.AppSettings["Node1-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node1-Y"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node2-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node2-Y"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node3-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node3-Y"]);
            if (System.Configuration.ConfigurationManager.AppSettings["Node4-ID"] == deviceID)
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["Node4-Y"]);

            return -1;
        }
    }
}
