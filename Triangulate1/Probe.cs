﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulate1
{
    class Probe
    {
        public MongoDB.Bson.BsonObjectId _id { get; set; }
        public DateTime  timestamp { get; set; }
        public string mac { get; set; }
        public string deviceid  { get; set; }
        public string ssid { get; set; }
        public int signalstrength { get; set; }
    }
}
