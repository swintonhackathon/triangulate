﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulate1
{
    class Location
    
    {
        public MongoDB.Bson.BsonObjectId _id { get; set; }
        
        public DateTime timestamp { get; set; }
        public string mac { get; set; }
        public int x { get; set; }
        public int y { get; set; }
    }
}
